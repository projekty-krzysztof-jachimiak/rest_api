import 'package:flutter/material.dart';
import 'package:testowanie_widget/services/api_manager.dart';

class HomePage extends StatefulWidget {
  final APIManager apiManager = APIManager();

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('News App'),
      ),
      body: Container(
        child: ListView.builder(itemBuilder: (context, index) {
          return Container(
            color: Colors.red,
            height: 100,
          );
        }),
      ),
    );
  }
}

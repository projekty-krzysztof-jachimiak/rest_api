import 'package:flutter/material.dart';

class User {
  String username;

  AssetImage profilePicture;
  List<User> followers;
  List<User> following;
  // List<Post> savedPosts;
  bool hasStory;

  User(this.username, this.profilePicture, this.followers, this.following,
      this.hasStory);
}

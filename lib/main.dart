import 'package:flutter/material.dart';
import 'package:testowanie_widget/home.dart';
import 'package:http/http.dart' as http;

void main() => runApp(MyApp());
void getNews() async {
  var client =
      http.Client(); //urzycie pakietu http wprowadzenie zmiennej client

  var respons = await client.get(
      'http://newsapi.org/v2/everything?domains=wsj.com&apiKey=e7b9ac270b4a4ea1880c439619d9c92f');
  // czyli mamy klienta http który chce wziąć  http i przekazać odpowiedź

  var jsonString = respons.body;
  debugPrint(respons.body);
  debugPrint(jsonString);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Instagram',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}
